using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelUnlock1 : MonoBehaviour
{


    public GUITexture[] levelArray;
    public Texture2D[] levelArray_tex;
    public Texture2D locked;
    private string str;

    void Start()
    {
        //PlayerPrefs.SetInt("world", 1);
		//PlayerPrefs.SetInt("levelSelected",0);
        for (int i = 0; i < levelArray.Length; i++)
        {

            if (i <= PlayerPrefs.GetInt("w" + WorldUnlock.num))
                levelArray[i].texture = levelArray_tex[i];
            else
                levelArray[i].texture = locked;
        }
    }

    void Update()
    {
        for (int i = 0; i < levelArray.Length; i++)
        {
            if (Input.GetMouseButton(0) && levelArray[i].HitTest(Input.mousePosition))

            {
				if (i <= PlayerPrefs.GetInt("w" + WorldUnlock.num))
                {

                    if (WorldUnlock.num == 1)
                        str = "w2";
                    Debug.Log(str + (i + 1));
					if(PlayerPrefs.GetInt("bossgame_value") == 2){
						PlayerPrefs.SetInt("bossgame_value", 1);
						SceneManager.LoadScene(0);
					}else{
						Application.LoadLevel(str + (i + 1));
					}
					Debug.Log(i);
					
					if(i == 0){ PlayerPrefs.SetInt("levelSelected",3); }
					else if(i == 1){ PlayerPrefs.SetInt("levelSelected",4); }
					else if(i == 2){ PlayerPrefs.SetInt("levelSelected",5); }
					else if(i == 3){ PlayerPrefs.SetInt("levelSelected",6); }
					else if(i == 4){ PlayerPrefs.SetInt("levelSelected",7); }
					else if(i == 5){ PlayerPrefs.SetInt("levelSelected",8); }
					else if(i == 6){ PlayerPrefs.SetInt("levelSelected",9); }
					else if(i == 7){ PlayerPrefs.SetInt("levelSelected",10); }
					else if(i == 8){ PlayerPrefs.SetInt("levelSelected",11); }
					else if(i == 9){ PlayerPrefs.SetInt("levelSelected",12); }

                }

            }
        }

    }
}