using UnityEngine;
using System.Collections;

public class CloseApp : MonoBehaviour {
 
    public GUITexture quit;
    public Texture2D[] quit_tex;
    public AudioClip click;

    void Start (){
    }

    void OnMouseUp (){
        quit.texture = quit_tex[0];
        Application.Quit();
    }
    void OnMouseDown (){
        GetComponent<AudioSource>().PlayOneShot(click);
        quit.texture = quit_tex[1];
        Login Udata = new Login();
        Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
        StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
        StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "17", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
        Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Application.Quit();
    }
}