using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class LBack : MonoBehaviour {

    public GUITexture wback;
    public Texture2D[] wback_tex;
    public AudioClip click;

    void Start (){

    }

    void Update (){

    }

    void  OnMouseUp (){
        wback.texture = wback_tex[0];
		SceneManager.LoadScene(1);
        //Application.LoadLevel("WorldSelection");
    }

    void  OnMouseDown (){
	    GetComponent<AudioSource>().PlayOneShot(click);
 	    wback.texture = wback_tex[1];
    }
}