using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {

//GUITexture pause;
    public GUITexture pauseMenu;
    public AudioClip click;
    
    void  Start (){

    }

    void  Update (){

    }

    void  OnMouseDown (){
        GetComponent<AudioSource>().PlayOneShot(click);
        Time.timeScale = 0;
        //pauseMenu.active = true;
        pauseMenu.gameObject.SetActive(true);
        //pause.transform.position=new Vector3(0.81f, 0.96f, 0);
    }
}