using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Replay : MonoBehaviour
{

    public GUITexture replay;
    public Texture2D[] replay_tex;
    public AudioClip click;
	
	IEnumerator postGetProfile2(string nickname)
    {
		Login lg = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", 17);

        UnityWebRequest www = UnityWebRequest.Post(lg.base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
        if (www.isNetworkError || www.isHttpError)
        {
			Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Profile Login");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
			Debug.Log(jsonObject["value"]);
			if(jsonObject["value"]==2){
			SceneManager.LoadScene(0);
			PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
			}else{
            
            if (jsonObject["value"] == 1)
            {
				PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
				PlayerPrefs.SetInt("bossgame_value", jsonObject["value"]);
				Debug.Log("hasil nyawa : "+PlayerPrefs.GetString("bossgame_heart"));
				SceneManager.LoadScene(13);
            }
			}
        }
    }
	
    void Start()
    {

    }

    void Update()
    {

    }
    void OnMouseUp()
    {
		PlayerPrefs.SetInt("btnLose",1);
		if (PlayerPrefs.GetString("statusAds") == "1")
		{
			replay.texture = replay_tex[0];
			Time.timeScale = 1;
			//if(PlayerPrefs.GetString("FirstPlayingGame")=="No"){
				Login lg = new Login();
				PlayerPrefs.SetInt("klikPlay",1);
				StartCoroutine(lg.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
				StartCoroutine(postGetProfile2(PlayerPrefs.GetString("bossgame_nickname")));
			/*BallsTrap bt = new BallsTrap();
			StartCoroutine(bt.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Forest Ball"));
			PlayerPrefs.SetString("bossgame_heart", (int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1).ToString());*/
			//}
			PlayerPrefs.SetString("FirstPlayingGame", "No");
		}else{
			/*BallsTrap bt = new BallsTrap();
			StartCoroutine(bt.postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Forest Ball"));
			PlayerPrefs.SetString("bossgame_heart", (int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1).ToString());*/
			replay.texture = replay_tex[0];
			Time.timeScale = 1;
			Application.LoadLevel(Application.loadedLevel);
			Debug.Log(PlayerPrefs.GetString("status_play"));
			if (PlayerPrefs.GetString("status_play") == "0")
			{
				SceneManager.LoadScene(0);
			}
		}

    }
    void OnMouseDown()
    {
        GetComponent<AudioSource>().PlayOneShot(click);
        replay.texture = replay_tex[1];

    }
}