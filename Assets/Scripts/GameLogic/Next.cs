using UnityEngine;
using System.Collections;

public class Next : MonoBehaviour
{

    public GUITexture next;
    public Texture2D[] next_tex;
    public AudioClip click;
    public int levelnum;

    void Start()
    {
    }

    void Update()
    {
    }
    void OnMouseUp()
    {

        next.texture = next_tex[0];
        Application.LoadLevel(levelnum);

    }
    void OnMouseDown()
    {
        GetComponent<AudioSource>().PlayOneShot(click);
        next.texture = next_tex[1];
    }
}