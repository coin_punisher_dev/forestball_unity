using UnityEngine;
using System.Collections;

public class WBack : MonoBehaviour {

    public GUITexture wback;
    public Texture2D[] wback_tex;
    public AudioClip click;

    void Start (){
    }

    void Update (){
    }

    void OnMouseUp (){
        wback.texture = wback_tex[0];
        Application.LoadLevel("Menu");
    }

    void OnMouseDown (){
	    GetComponent<AudioSource>().PlayOneShot(click);
 	    wback.texture = wback_tex[1];
    }
}