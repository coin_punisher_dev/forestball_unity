using UnityEngine;
using System.Collections;

public class Resume : MonoBehaviour
{

    public GUITexture resume;
    public Texture2D[] resume_tex;
    public GUITexture pauseMenu;
    public AudioClip click;

    void Start()
    {

    }

    void Update()
    {

    }

    void OnMouseUp()
    {
        resume.texture = resume_tex[0];
        Time.timeScale = 1;
        //pauseMenu.active = false;
        pauseMenu.gameObject.SetActive(false);


    }
    void OnMouseDown()
    {
        GetComponent<AudioSource>().PlayOneShot(click);
        resume.texture = resume_tex[1];



    }
}