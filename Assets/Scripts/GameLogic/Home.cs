using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Home : MonoBehaviour
{

    public GUITexture home;
    public Texture2D[] home_tex;
    public AudioClip click;

    void Start()
    {

    }

    void Update()
    {

    }
    void OnMouseUp()
    {	
		PlayerPrefs.SetInt("btnLose",2);
		if (PlayerPrefs.GetString("statusAds") == "1")
		{
			home.texture = home_tex[0];
			Time.timeScale = 1;
			PlayerPrefs.SetInt("levelSelected",2);
			SceneManager.LoadScene(2);
		}else{
			home.texture = home_tex[0];
			Time.timeScale = 1;
			PlayerPrefs.SetInt("levelSelected",2);
			SceneManager.LoadScene(2);
		}

    }
    void OnMouseDown()
    {
        GetComponent<AudioSource>().PlayOneShot(click);
        home.texture = home_tex[1];

    }
}