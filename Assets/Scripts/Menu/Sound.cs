using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {
 
    public GUITexture sound;
    public Texture2D[] sound_tex;

    public GameObject internet_lose, logo, play;
    public int status_internet;

    public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    void Start (){

        PlayerPrefs.SetString("FirstPlay", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Screen.SetResolution(480, 800, true);

        this.status_internet = 1;
        if (AudioListener.pause){
            sound.texture = sound_tex[1];
        }
        if (this.status_internet == 0)
        {
            internet_lose.SetActive(true);
            logo.SetActive(false);
            play.SetActive(false);
        }
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected)
            {
                internet_lose.SetActive(true);
                logo.SetActive(false);
                play.SetActive(false);
            }
        }));

    }

    void Update (){
    }

    void OnMouseDown (){
        if(sound.texture.name=="SPEAKER"){
            AudioListener.pause = true;
            sound.texture = sound_tex[1];
        }
        else if(sound.texture.name=="MUTE"){
            AudioListener.pause = false;
            sound.texture = sound_tex[0];
        }
    }
}