using UnityEngine;
using System.Collections;

public class MenuPlay : MonoBehaviour {

    public GUITexture play;
    public Texture2D[] play_tex;
    public AudioClip click;
    public GameObject loginPage, logo, playbo;

    void Start (){
    }

    void Update (){
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }
    void OnMouseUp (){
        play.texture = play_tex[0];
        loginPage.SetActive(true);
        logo.SetActive(false);
        playbo.SetActive(false);
        //Application.LoadLevel("WorldSelection");
    }
    void OnMouseDown (){
        GetComponent<AudioSource>().PlayOneShot(click);
        play.texture = play_tex[1];
    }
}