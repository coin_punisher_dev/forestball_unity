using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
//using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using System;
using SimpleJSON;

public class BallsTrap : MonoBehaviour
{
    //private RewardBasedVideoAd reward_based_ad;
    //private int statusAds = 0;
	

    public static bool _gameOver = false;

    public static bool _win = false;

    public BezierPathController PathController;

    public AnimationManager AnimationManager;

    public MusicManager MusicManager;
	
	public GameObject GameOverMenu;
	
	public GameObject GameWonMenu;

    public Text heart, sisa_main_2_message;
	
	public IEnumerator PostRunningAds(string _param, string _message, int _ads, double _amount)
    {
		Login Udata = new Login();
        WWWForm form = new WWWForm();

        form.AddField("memberID", _param);
        form.AddField("gameID", "17");
        form.AddField("message", _message);
        form.AddField("ads", _ads);
        form.AddField("amount", _amount.ToString());

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "runningAds.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }

    }

    IEnumerator postGetProfile(string nickname)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", "17");

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "cekNickName.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            Debug.Log(a);
            var jsonObject = JSON.Parse(a);
            Udata.bossgame_iduser = jsonObject["id"];
            Udata.bossgame_email = jsonObject["email"];
            Udata.bossgame_username = jsonObject["username"];
            PlayerPrefs.SetString("bossgame_username", jsonObject["username"]);
            Udata.bossgame_avatar = jsonObject["photo"];
            PlayerPrefs.SetString("bossgame_avatar", jsonObject["photo"]);
            Udata.bossgame_idvalue = jsonObject["value"];
            Udata.bossgame_valmessage = jsonObject["message"];
            PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);

        }
    }

    public IEnumerator postChancePlaying(string nickname, string game_name)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("namaGame", "Rocket Adventure");

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url.ToString() + "lifeMinusGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
            var jsonObject = JSON.Parse(a);
            Debug.Log(jsonObject["value"] + " - " + jsonObject["message"]);
            if (jsonObject["value"] == 0)
            {
                //this.OnBackToHome();
                Udata.statusPlay = 0;
                PlayerPrefs.SetString("status_play","0");
                //sisa_main_2_message.text = "";
            }
            else if (jsonObject["value"] == 1)
            {
                if (jsonObject["heart"] <= 0)
                {
                    //heart.text = "0";
                    //sisa_main_2_message.text = "Kesempatan Bermain Telah Habis";
                    Udata.statusPlay = 0;
                    PlayerPrefs.SetString("status_play", "0");
                    Debug.Log("Nol");
                }
                else
                {
                    //heart.text = jsonObject["heart"];
                    //sisa_main_2_message.text = "";
                    Udata.statusPlay = 1;
                    PlayerPrefs.SetString("status_play", "1");
                    Debug.Log("Satu");
                }
            }
            else if (jsonObject["value"] == 2)
            {
                //heart.text = jsonObject["heart"];
                //sisa_main_2_message.text = jsonObject["message"];
                Udata.statusPlay = 0;
                PlayerPrefs.SetString("status_play", "0");
            }

        }
    }

    void Start()
    {
        //InitGoogleAd();
        //this.statusAds = 0;
		
		Login Udata = new Login();
		PlayerPrefs.SetString("statusAds","0");
		Debug.Log(PlayerPrefs.GetString("statusAds"));
        Advertisement.Initialize ("3437561", false);
        StartCoroutine(CheckWinCoroutine());
    }
/*
    void InitGoogleAd()
    {
        // #if UNITY_ANDROID
        // 	string app_id = "ca-app-pub-3940256099942544~3347511713";
        // #elif UNITY_IPHONE
        // 	string app_id = "ca-app-pub-3940256099942544~1458002511";
        // #else
        // 	string app_id = "unexpected_platform";
        // #endif

        //production
        //string app_id = "ca-app-pub-2705296754237795~2977351041";

        //sample
        string app_id = "ca-app-pub-3940256099942544~3347511713";

        MobileAds.Initialize(app_id);

        this.reward_based_ad = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        reward_based_ad.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        reward_based_ad.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        reward_based_ad.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        reward_based_ad.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        reward_based_ad.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        reward_based_ad.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        reward_based_ad.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;


        this.RequestRewardBasedVideo();

    }
    private void RequestRewardBasedVideo()
    {
        // #if UNITY_ANDROID
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";
        // #elif UNITY_IPHONE
        // 	string ad_unit_id = "ca-app-pub-3940256099942544/1712485313";
        // #else
        // 	string ad_unit_id = "unexpected_platform";
        // #endif

        //production
        //string ad_unit_id = "ca-app-pub-2705296754237795/5411942695";

        //sample
        string ad_unit_id = "ca-app-pub-3940256099942544/5224354917";

        AdRequest request = new AdRequest.Builder().Build();

        this.reward_based_ad.LoadAd(request, ad_unit_id);
    }
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoLoaded", 1, 0.0));
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoFailedToLoad", 2, 0.0));
    }
    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoOpened", 3, 0.0));
    }
    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoStarted", 4, 0.0));
    }
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoClosed", 5, 0.0));
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);

        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoRewarded", 6, amount));
    }
    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");

        StartCoroutine(PostRunningAds(PlayerPrefs.GetString("bossgame_nickname"), "HandleRewardBasedVideoLeftApplication", 7, 0.0));
    }

*/
    void OnGUI()
    {
        /*if (reward_based_ad.IsLoaded())
        {
            if (this.statusAds == 1)
            {
                Debug.Log("show iklan");
                reward_based_ad.Show();
            }

        }*/
    }

    private IEnumerator CheckWinCoroutine()
    {
        while (true)
        {
            if (PathController.Factory.GeneratedBalls == PathController.Factory.GeneratedBallsAmount &&
                PathController.BallSequence.Count == 0 && !_gameOver)
            {
                _win = true;
				GameWonMenu.active = true;
                MusicManager.Win();
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Sound sound = new Sound();
        if (other.gameObject.tag == "Ball")
        {
            var ball = other.GetComponent<Ball>();
            if (PathController.BallSequence.Contains(ball))
            {
                PathController.DestroyBall(ball);
            }
        }

        ///Check to loose;
        if (other.gameObject.tag == "Ball" && PathController.BallSequence.Contains(other.GetComponent<Ball>()) && _gameOver == false)
        {
            StartCoroutine(sound.checkInternetConnection((isConnected) => {
                if (!isConnected)
                {
                    sound.status_internet = 0;
                    SceneManager.LoadScene(0);
                }
            }));
            //this.statusAds = 1;
			PlayerPrefs.SetString("statusAds","1");
			//StartCoroutine(postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Forest Ball"));
			//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
			//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
            //Login Udata = new Login();
            //LoadProfileAndHealth LPAH = new LoadProfileAndHealth();
            
			
			/*StartCoroutine(postChancePlaying(PlayerPrefs.GetString("bossgame_nickname"), "Rocket Adventure"));
			int totaljumlah = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
			PlayerPrefs.SetString("bossgame_heart", totaljumlah.ToString());*/
            
			//StartCoroutine(postGetProfile(PlayerPrefs.GetString("bossgame_nickname").ToString()));
            //LPAH.heart.text = PlayerPrefs.GetString("bossgame_heart");
            MusicManager.Loose();
            PathController.StopSequence();
            AnimationManager.RunAnimation(AnimationThrowType.OnGameOver);
            _gameOver = true;
			GameOverMenu.active = true;
            PathController.MoveToDestroyAll();
        }
    }

//    private void OnGUI()
//    {
//        if (_gameOver)
//        {
//            GUI.Label(new Rect(Screen.width/2, Screen.height/2, 200, 50), "GameOver!");
//        }
//        if (_win)
//        {
//            GUI.Label(new Rect(Screen.width/2, Screen.height/2, 200, 50), "Win!");
//        }
//    }
}
